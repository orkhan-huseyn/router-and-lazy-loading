import { NavLink, Outlet } from 'react-router-dom'

function DefaultLayout() {
  return (
    <>
      <header>
        <h1>Logo</h1>
        <nav>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contact">Contact</NavLink>
          <NavLink to="/users">Users</NavLink>
        </nav>
      </header>
      <main>
        <Outlet />
      </main>
      <footer>(c)2022 ATL Acamdemy</footer>
    </>
  )
}

export default DefaultLayout
