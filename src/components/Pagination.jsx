import React, { useState } from 'react'

function Pagination({
  total,
  pageSize,
  defaultCurrentPage,
  onPageChange,
  prevLabel,
  nextLabel,
}) {
  const [currentPage, setCurrentPage] = useState(defaultCurrentPage)
  const pageCount = total / pageSize
  const pages = Array.from({ length: pageCount }, (_, i) => i + 1)

  const isFirstPage = currentPage === 1
  const isLastPage = currentPage === pageCount

  function handlePageClick(page) {
    setCurrentPage(page)
    onPageChange && onPageChange(page)
  }

  function handlePrevClick() {
    const prevPage = currentPage - 1
    setCurrentPage(prevPage)
    onPageChange && onPageChange(prevPage)
  }

  function handleNextClick() {
    const nextPage = currentPage + 1
    setCurrentPage(nextPage)
    onPageChange && onPageChange(nextPage)
  }

  return (
    <nav>
      <ul className="pagination">
        <li className={`page-item ${isFirstPage ? 'disabled' : ''}`}>
          <button onClick={handlePrevClick} className="page-link">
            {prevLabel}
          </button>
        </li>

        {pages.map((pageNum) => (
          <li
            key={pageNum}
            className={`page-item ${currentPage === pageNum ? 'active' : ''}`}
          >
            <button
              onClick={() => handlePageClick(pageNum)}
              className="page-link"
            >
              {pageNum}
            </button>
          </li>
        ))}

        <li className={`page-item ${isLastPage ? 'disabled' : ''}`}>
          <button onClick={handleNextClick} className="page-link">
            {nextLabel}
          </button>
        </li>
      </ul>
    </nav>
  )
}

Pagination.defaultProps = {
  total: 0,
  pageSize: 10,
  defaultCurrentPage: 1,
  prevLabel: '<<',
  nextLabel: '>>',
}

export default Pagination
