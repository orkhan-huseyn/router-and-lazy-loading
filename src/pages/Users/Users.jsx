import { useEffect } from 'react'
import { useState } from 'react'
import { Link, useSearchParams } from 'react-router-dom'
import axios from 'axios'
import Pagination from '../../components/Pagination'

function Users() {
  const [users, setUsers] = useState([])
  const [searchParams, setSearchParams] = useSearchParams()

  const [page, setPage] = useState(
    searchParams.has('page') ? searchParams.get('page') : 1
  )

  useEffect(() => {
    const skip = (page - 1) * 30
    axios
      .get(`https://dummyjson.com/users?skip=${skip}`)
      .then((data) => setUsers(data.data.users))
  }, [page])

  function handlePageChange(page) {
    setPage(page)
    setSearchParams({ page })
  }

  return (
    <>
      <h1>Users page</h1>
      <ul>
        {users.map((user) => (
          <li key={user.id}>
            {user.firstName} {user.lastName}{' '}
            <Link to={`/profile/${user.id}`}>(@{user.username})</Link>
          </li>
        ))}
      </ul>
      <Pagination pageSize={30} total={100} onPageChange={handlePageChange} />
    </>
  )
}

export default Users
