import React from 'react'

const LazyUsers = React.lazy(() => import('./Users'))

export default () => (
  <React.Suspense fallback={<h1>Importing users page...</h1>}>
    <LazyUsers />
  </React.Suspense>
)
