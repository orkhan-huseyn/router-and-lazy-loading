import React from 'react'

const LazyProfile = React.lazy(() => import('./Profile'))

export default () => (
  <React.Suspense fallback={<h1>Importing profile page...</h1>}>
    <LazyProfile />
  </React.Suspense>
)
