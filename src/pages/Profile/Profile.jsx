import { useState } from 'react'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

function Profile() {
  const { userId } = useParams()
  const [user, setUser] = useState({})

  useEffect(() => {
    fetch(`https://dummyjson.com/users/${userId}`)
      .then((res) => res.json())
      .then((data) => setUser(data))
  }, [])

  return (
    <>
      <h1>
        {user.firstName} {user.lastName}
      </h1>
      <img width="100px" src={user.image} alt={user.username} />
    </>
  )
}

export default Profile
