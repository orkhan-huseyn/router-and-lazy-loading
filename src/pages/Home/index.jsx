import React from 'react'

const LazyHome = React.lazy(() => import('./Home'))

export default () => (
  <React.Suspense fallback={<h1>Importing home page...</h1>}>
    <LazyHome />
  </React.Suspense>
)
